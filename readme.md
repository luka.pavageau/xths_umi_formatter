# xths_formatter

This program is made to extract UMIs into read headers for reads sequenced with the Agilent Sureselect XT-HS kit.

It relies on the fact that UMIs are extracted during the demultiplexing in an additional fastq file, using the following command : 
 
```sh
bcl2fastq \
    -R $bcl_folder \
    --sample-sheet $samplesheet \
    --no-lane-splitting \
    -o $output_folder \
    --barcode-mismatches 1 \
    -p $threads -r $threads -w $threads \
    --mask-short-adapter-reads 0 \
    --no-bgzf-compression \
    --use-bases-mask "Y*, I8, Y10, Y*"
```

This generates 3 files by sample : 
```
{sample}_R1*.fq.gz  : read1
{sample}_R2*.fq.gz  : UMIs
{sample}_R3*.fq.gz  : read2
```

## Installation

To compile from source, use rustup and cargo : https://www.rust-lang.org/tools/install

```sh
git clone https://gitlab.com/luka.pavageau/xths_umi_formatter.git
cd xths_umi_formatter
cargo build --release
```

Pre-compiled binaries are available in ./bin

## Usage 
```
xths_formatter \
    --read1 <read1> \
    --umi <umi>
    --read2 <read2> \
    --read1-out <read1-out> \
    --read2-out <read2-out> \

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
        --read1 <read1>            path to the R1 fastq
        --read1-out <read1-out>    where to write the read1.fastq
        --read2 <read2>            path to the R2 fastq, labelled by the demultiplexing as {sample}_R3*.fastq.gz
        --read2-out <read2-out>    where to write the read2.fastq
        --umi <umi>                path to the UMIs fastq, labelled by the demultiplexing as {sample}_R2*.fastq.gz
```

The `read1-out` and `read2-out` files are uncompressed fastqs and can be compressed again with the `bgzip` command.