use structopt::StructOpt;

mod functions;
use functions::convert_fastqs;

#[derive(Debug, StructOpt)]
#[structopt(verbatim_doc_comment)] 
///This program is made to extract UMIs into read headers for reads sequenced with the Agilent Sureselect XT-HS kit.
///
///It relies on the fact that UMIs are extracted during the demultiplexing in an additional fastq file, using the following command : 
/// 
///```sh
///bcl2fastq \
///    -R $bcl_folder \
///    --sample-sheet $samplesheet \
///    --no-lane-splitting \
///    -o $output_folder \
///    --barcode-mismatches 1 \
///    -p $threads -r $threads -w $threads \
///    --mask-short-adapter-reads 0 \
///    --no-bgzf-compression \
///    --use-bases-mask "Y*, I8, Y10, Y*"
///```
struct Cli {
    /// path to the R1 fastq
    #[structopt(long, parse(from_os_str))]
    read1: std::path::PathBuf,

    /// path to the UMIs fastq, labelled by the demultiplexing as {sample}_R2*.fastq.gz
    #[structopt(long, parse(from_os_str))]
    umi: std::path::PathBuf,

    /// path to the R2 fastq, labelled by the demultiplexing as {sample}_R3*.fastq.gz
    #[structopt(long, parse(from_os_str))]
    read2: std::path::PathBuf,
    
    /// where to write the read1.fastq
    #[structopt(long, parse(from_os_str))]
    read1_out: std::path::PathBuf,

    /// where to write the read2.fastq
    #[structopt(long, parse(from_os_str))]
    read2_out:std::path::PathBuf,

}

/// main function parsing the arguments and sending them to the appropriate subcommands
fn main() -> () {

    let args = Cli::from_args();

    convert_fastqs(
        args.read1, 
        args.umi, 
        args.read2, 
        args.read1_out, 
        args.read2_out
    )
}