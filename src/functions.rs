use std::io::{prelude::*, BufReader};
use std::fs::File;
use itertools::izip;
use rust_htslib::bgzf::Reader;
use std::io::BufWriter;

/// ajoute le barcode moléculaire au header fastq
fn append_umi_header(s : & String, umi:&str) -> String {
    let parts_of_header : Vec<&str> = s.split(" ").collect();

    if let [a, b] = &parts_of_header[..] {
        let new_str = format!("{}:UMI_{} {}", a, umi, b);
        return  new_str;
    }else {
        panic!("Un header est mal formaté : {:?}", s)
    }
}

/// Takes 5 paths as inputs :
/// - r1 : path to the R1 fastq
/// - umi : path to the UMIs fastq, labelled by the demultiplexing as R2
/// - r2 : path to the R2 fastq, labelled R3
pub fn convert_fastqs(
    r1:std::path::PathBuf, 
    umi:std::path::PathBuf, 
    r2:std::path::PathBuf, 
    read1_out:std::path::PathBuf, 
    read2_out:std::path::PathBuf
) {

    // Creating 2 writers for the 2 fastq outputs
    let mut read1_out = BufWriter::new(
        File::create(read1_out).unwrap()
    );
    let mut read2_out = BufWriter::new(
        File::create(read2_out).unwrap()
    );

    // 2 string buffers that will countain 1 fastq entry and be replaced every 4 cycles of the loop
    let mut buff_read1 = String::new();
    let mut buff_read2 = String::new();
    
    // the readers for the input fastqs
    // they will be read in parallel on each of their lines
    let r1_reader = BufReader::new(Reader::from_path(r1).unwrap());
    let umi_reader = BufReader::new(Reader::from_path(umi).unwrap());
    let r2_reader = BufReader::new(Reader::from_path(r2).unwrap());

    // line counter
    let mut i = 0;

    // entries counter
    let mut entr : f64 = 0.0;
    // malformed entries counter
    let mut malf : f64 = 0.0;

    // reading in parallel the lines of each of the 3 files
    for (l_r1, l_umi, l_r2) in izip!(
        r1_reader.lines(),
        umi_reader.lines(),
        r2_reader.lines()
        )
    {
        // first line of a fastq entry : the header
        // this is the line that we want to modify, so we add it to our fastq buffer
        if i%4 == 0 {
            if i != 0 {
                read1_out.write(&buff_read1.as_bytes()).unwrap();
                read2_out.write(&buff_read2.as_bytes()).unwrap();
            }
            buff_read1 = l_r1.unwrap() + "\n";
            buff_read2 = l_r2.unwrap() + "\n";
            entr += 1.0;
        } 
        // second line of the fastq entry : the sequence
        // we want the UMI sequence to be added to the header with the ":UMI_{seq}" tag
        else if (i -1)%4 == 0 {
            let my_umi = l_umi.unwrap();
            
            if my_umi == "NNNNNNNNNN" {
                malf += 1.0
            }
            
            buff_read1 = append_umi_header(&buff_read1, &my_umi);
            buff_read2 = append_umi_header(&buff_read2, &my_umi);
            
            buff_read1 += &format!("{}\n", l_r1.unwrap());
            buff_read2 += &format!("{}\n", l_r2.unwrap());
        }
        // everything else stay the same (the line with "+" and the quality sequence)
        else {
            buff_read1 += &format!("{}\n", l_r1.unwrap());
            buff_read2 += &format!("{}\n", l_r2.unwrap());
        }
        i += 1;
    }
    if malf/entr > 0.1 {
        panic!("There is more than 10% {} of 'NNNNNNNNN' sequences for the UMIs, there is a problem with the demultiplexing. Check you samplesheet and your bcl2fastq command line.", malf/entr)
    }
}